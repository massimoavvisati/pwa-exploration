import QrScanner from 'https://cdn.jsdelivr.net/npm/qr-scanner@1.4.2/+esm'

if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
        navigator.serviceWorker.register('/service-worker.js')
            .then(registration => {
                //console.log('Service Worker registrato con successo:', registration);
            })
            .catch(error => {
                console.log('Registrazione del Service Worker fallita:', error);
            });
    });
}

var lastResult = '';

const startButton = document.getElementById('start-button');
const historyDialog = document.getElementById('history-list');
const videoElem = document.querySelector('video');

const gotResult = (result) => {
    if (result.data != lastResult)  {
        
        const resultContainer = document.getElementById('result-container');
        lastResult = result.data;
        const newMessageElem = document.createElement('div');
        newMessageElem.classList.add('result-content');
        newMessageElem.innerHTML = result.data;
        resultContainer.appendChild(newMessageElem);
        const animation = newMessageElem.animate(
            [
                { opacity: 0 },
                { opacity: 1, offset: 0.3 },
                { opacity: 1, offset: 0.9 }
            ],
            {
                duration: 2000,
                fill: 'forwards'
            }
        );
    
        animation.onfinish = () => {
            console.log('L\'animazione è terminata');
            newMessageElem.classList.remove('result-content');
            historyDialog.appendChild(newMessageElem);
            
        };
  
    }
    //console.log(result)
}

const qrScanner = new QrScanner(
    videoElem,
    gotResult,
    { /* your options or returnDetailedScanResult: true if you're not specifying any other options */ },
);

startButton.addEventListener('click', startScanning);
startButton.addEventListener('touch', startScanning);


function startScanning() {

    document.getElementById('splash-screen').style.opacity = 0;

    
    qrScanner.start();
    

}